# Programma di Fisica (9 CFU)
### Anno Accademico 2016-2017

## Introduzione

- Grandezza fisica
- Unità di misura
- Sistema internazionale
- Equazione dimensionale
- Errori di misura
- Approssimazione
- Vettore per uno scalare
- Prodotto scalare
- Prodotto vettoriale
- Componenti di un vettore

## Cinematica

- Velocità
- Accelerazione
- Legge oraria
- Moto rettilineo uniforme
- Moto rettilineo uniformemente accelerato
- Moto nel piano
- Moto parabolico
- Moti circolari
- Cinematica dei moti circolari
- Moto circolare uniforme

## Dinamica del punto materiale

- Principio di inerzia
- Massa inerziale
- Forza: II legge di Newton
- Principio di azione e reazione
- Leggi della forza: forza gravitazionale, forza peso, forza di attrito, forze elastiche
- Reazioni vincolari
- Tensione dei fili
- Diagramma del corpo libero
- Moto lungo un piano inclinato liscio e scabro
- Moti circolari: Forze centripete
- Quantità di moto
- Impulso
- *Momento angolare (facoltativo)*
- *Momento meccanico (facoltativo)*

## Conservazione dell'energia

- Lavoro
- Energia cinetica
- Teorema delle forze vive
- Forze conservative
- Energia potenziale
- Calcolo di energia potenziale
- Conservazione dell'energia meccanica
- Forze non conservative

## Dinamica dei sistemi di punti materiali

- Sistemi di punti: forze interne e forze esterne
- Centro di massa di un sistema di punti
- Teorema del moto del centro di massa
- Conservazione della quantità di moto
- Urti tra punti materiali: elastico, anelastico, completamente anelastico

## Oscillazioni

- Oscillatore armonico semplice: equazione del moto e soluzione
- Sistema massa-molla
- *Pendolo semplice (facoltativo)*
- Energia cinetica e potenziale nei moti armonici semplici
- Oscillatore armonico smorzato da una forza viscosa: smorzamento debole, *forte e critico (facoltativi)*
- *Oscillatore armonico forzato (facoltativo)*
- *Risonanza (facoltativo)*

## Campo elettrostatico

- Il fenomeno dell'elettrizzazione
- La carica elettrica: proprietà
- Isolanti e conduttori
- La legge di Coulomb
- Campo elettrico per cariche in quiete
- Principio di sovrapposizione
- Campo di un dipolo elettrostatico
- Moto di una particella carica in un campo elettrico uniforme
- Linee di forza del campo elettrico
- Flusso del campo elettrico
- Legge di Gauss
- Applicazione ad isolanti carichi
- Applicazione a conduttori in equilibrio elettrostatico
- Potenziale elettrico e differenza di potenziale
- Energia di una carica puntiforme in un campo elettrostatico
- Energia potenziale di un sistema di cariche
- Capacità di un conduttore isolato
- Induzione elettrostatica
- Condensatore
- Capacità di un condensatore
- Il condensatore piano
- Collegamento di condensatori
- *Condenzatore con dielettrico (facoltativo)*
- *La costante dielettrica relativa (facoltativo)*
- Energia immagazzinata nel campo elettrico

## Corrente elettrica continua

- Corrente elettrica
- Resistenza e legge di Ohm
- Resistività dei conduttori
- Densità di corrente
- Effetto Joule
- Forza elettromotrice e generatori di tensione
- Leggi di Kirchhoff
- Resistenze in serie e in parallelo
- Circuito RC

## Campo magnetostatico

- Definizione di campo magnetico B
- Proprietà del campo magnetico
- Forza di Lorentz
- *Effetto Hall (facoltativo)*
- Moto di una particella carica in un campo magnetico costante
- Forza magnetica su un conduttore percorso da corrente
- *Seconda formula di Laplace (facoltativo)*
- *Momento agente su una spira percorsa da corrente (facoltativo)*
- Legge di Biot-Savart
- *Prima formula di Laplace (facoltativo)*
- Forze magnetiche tra due conduttori paralleli
- Definizione di ampere
- Legge di Ampère
- Campo magnetico di un solenoide ideale
- Flusso magnetico
- Legge di Gauss per il campo magnetico

## Campo elettromagnetico variabile nel tempo

- Induzione elettromagnetica
- Legge di Lenz-Faraday-Neumann
- Forze elettromotrici indotte e campi elettrici
- Autoinduzione
- Circuiti RL
- Energia in un campo magnetico
- *Oscillazioni in un circuito LC (facoltativo)*
- Corrente di spostamento: Equazione di Ampère-Maxwell
- Forma integrale delle equazioni di Maxwell
- *Onde elettromagnetiche (facoltativo)*
